package foodtruckmovement.ftmvendor.ui.login

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.amazonaws.auth.AWSCognitoIdentityProvider
import com.amazonaws.services.cognitoidentityprovider.model.AdminGetUserRequest
import com.amazonaws.services.cognitoidentityprovider.model.GetUserResult
import com.amplifyframework.auth.AuthCategory
import com.amplifyframework.auth.AuthChannelEventName
import com.amplifyframework.auth.cognito.AWSCognitoAuthSession
import com.amplifyframework.auth.result.AuthSessionResult
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.InitializationStatus
import com.amplifyframework.hub.HubChannel
import com.amplifyframework.hub.HubEvent
import com.foodtruckmovement.ftmvendor.R
import foodtruckmovement.ftmvendor.ui.checkIn.CheckInActivity
import okhttp3.*
import okhttp3.FormBody
import org.json.JSONObject
import java.io.IOException


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val sharedPreferences = this?.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        val authToken = sharedPreferences.getString(getString(R.string.ftm_user_token), "")

        if (authToken != "") {
            val intent = Intent(this, CheckInActivity::class.java)
            startActivity(intent)
            finish()
        }

        Amplify.Hub.subscribe(HubChannel.AUTH) { hubEvent: HubEvent<*> ->
            if (hubEvent.name == InitializationStatus.SUCCEEDED.toString()) {
                Log.i("AuthQuickstart", "Auth successfully initialized")
            } else if (hubEvent.name == InitializationStatus.FAILED.toString()) {
                Log.i("AuthQuickstart", "Auth failed to succeed")
            } else {
                when (AuthChannelEventName.valueOf(hubEvent.name)) {
                    AuthChannelEventName.SIGNED_IN -> Log.i("AuthQuickstart", "Auth just became signed in.")
                    AuthChannelEventName.SIGNED_OUT -> Log.i("AuthQuickstart", "Auth just became signed out.")
                    AuthChannelEventName.SESSION_EXPIRED -> Log.i("AuthQuickstart", "Auth session just expired.")
                }
            }
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN) {
            if (currentFocus != null) {
                if (currentFocus is EditText) {
                    val outRect = Rect()
                    currentFocus.getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        if (inputMethodManager.isAcceptingText)
                            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        currentFocus.clearFocus()
                    }
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    fun login(view: View) {
        val username = findViewById<EditText>(R.id.username_field).text.toString()
        val password = findViewById<EditText>(R.id.password_field).text.toString()

        Amplify.Auth.signIn(
                username,
                password,
                { result ->
                    if (result.isSignInComplete) {
                        Log.e("result", result.toString())
                        Amplify.Auth.fetchAuthSession(
                                { result ->
                                run {
                                    val cognitoAuthSession = result as AWSCognitoAuthSession
                                    when (cognitoAuthSession.identityId.type) {
                                        AuthSessionResult.Type.SUCCESS -> {
                                            val token = cognitoAuthSession.userPoolTokens.value?.idToken
                                            val sharedPreferences = this@LoginActivity?.getSharedPreferences(
                                                    getString(R.string.preference_file_key), Context.MODE_PRIVATE)
                                            with (sharedPreferences.edit()) {
                                                putString(getString(R.string.ftm_user_token), token)
                                                commit()
                                            }
                                            val intent = Intent(this@LoginActivity, CheckInActivity::class.java)
                                            startActivity(intent)
                                            finish()
                                        }
                                        AuthSessionResult.Type.FAILURE -> Log.i("AuthQuickStart", "IdentityId not present because: " + cognitoAuthSession.identityId.error.toString())
                                    }
                                 }
                                },
                                { error -> Log.e("fetch auth session", error.toString())
                                }
                        )
                    } else {
                        "Sign in not complete"
                    }
                },
                {
                    error -> Log.e("AuthQuickstart", error.toString())
                }
        )

        /*val client = OkHttpClient()

        val formBody: RequestBody = FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .build()

        val post: Request = Request.Builder()
                .url(getString(R.string.domain) + "/api/validate-credentials/")
                .post(formBody)
                .build()

        client.newCall(post).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    kotlin.run {
                        Toast.makeText(this@LoginActivity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onResponse(call: Call, response: Response) {
                try {
                    val responseBody: ResponseBody? = response.body
                    if (!response.isSuccessful) {
                        throw IOException("Unexpected code $response")
                    }
                    if (responseBody != null) {
                        val json = JSONObject(responseBody.string())
                        val token = json.getString("token")
                        val sharedPreferences = this@LoginActivity?.getSharedPreferences(
                                getString(R.string.preference_file_key), Context.MODE_PRIVATE)
                        with (sharedPreferences.edit()) {
                            putString(getString(R.string.ftm_user_token), token)
                            commit()
                        }
                        val intent = Intent(this@LoginActivity, CheckInActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })*/

    }

}

