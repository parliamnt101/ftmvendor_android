package foodtruckmovement.ftmvendor.ui.profile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.net.UrlQuerySanitizer
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.view.View
import android.view.View.OnTouchListener
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.foodtruckmovement.ftmvendor.R
import com.squareup.picasso.Picasso
import foodtruckmovement.ftmvendor.ui.checkIn.CheckInActivity
import foodtruckmovement.ftmvendor.ui.login.LoginActivity
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import lib.AuthStateManager
import lib.AuthenticationService
import net.openid.appauth.AuthState
import net.openid.appauth.AuthorizationRequest
import net.openid.appauth.AuthorizationService
import net.openid.appauth.AuthorizationServiceConfiguration
import okhttp3.*
import okhttp3.MediaType.Companion.get
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Retrofit
import java.io.File
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicReference
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ProfileEditActivity : AppCompatActivity() {

    private lateinit var mAuthStateManager: AuthStateManager

    private var authToken = ""
    private var truckName = ""
    private var truckDescription = ""
    private var truckWebsite = ""
    private var truckImage = ""
    private var truckTwitterAccount = ""
    private var newTruckImage : Uri? = null

    private var twitterAccessToken : String = ""
    private val redirectUrl = "ftmvendor://oauth-twitter-callback.com"
    private val requestTokenUrl = "https://api.twitter.com/oauth/request_token"
    private val authorizeUrl = "https://api.twitter.com/oauth/authorize"
    private val accessTokenUrl= "https://api.twitter.com/oauth/access_token"

    private val mClientId = AtomicReference<String>()
    private val mAuthRequest = AtomicReference<AuthorizationRequest>()
    private var mExecutor: ExecutorService? = null
    private var mAuthService: AuthorizationService? = null
    private val mAuthIntent = AtomicReference<CustomTabsIntent>()


    private var authServerObserver = object : Observer<Boolean> {

        override fun onSubscribe(d: Disposable) {}

        override fun onNext(s: Boolean) {
            if(s){
                startAuth()
            }
        }

        override fun onError(e: Throwable) {}

        override fun onComplete() {}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 3 && resultCode == Activity.RESULT_OK) {
            val selectedImage = data?.data
            val truckImageView: ImageView = findViewById(R.id.truck_image)
            Picasso.get().load(selectedImage).into(truckImageView)
            newTruckImage = selectedImage
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)

        val view = findViewById<LinearLayout>(R.id.profile_edit_base_view)

        mExecutor = Executors.newSingleThreadExecutor()
        mAuthStateManager = AuthStateManager.getInstance(this)

        val action: String? = intent?.action
        val data: Uri? = intent?.data

        if (action != null && data != null) {

            val sanitizer = UrlQuerySanitizer(data.toString())
            val token = sanitizer.getValue("oauth_token")
            val verifier = sanitizer.getValue("oauth_verifier")

            val extraParams = HashMap<String, String>()
            extraParams["oauth_token"] = token
            extraParams["oauth_verifier"] = verifier

            val oauthHeader = generateOAuthAuthorizationHeader(extraParams)

            val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.twitter.com/")
                    .build()
            retrofit.callbackExecutor()
            val service = retrofit.create(AuthenticationService::class.java)
            val call = service.getAccessToken(oauthHeader)
            call.enqueue( object: retrofit2.Callback<ResponseBody> {
                override fun onResponse(call: retrofit2.Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) =
                        if (response.isSuccessful) {
                            val responseBody = response.body()!!.string()
                            val response = response.toString()
                            val uri = Uri.parse("http://foo.bar?$responseBody")
                            val params = HashMap<String, Any>()
                            params["token"] = uri.getQueryParameter("oauth_token")
                            params["secret"] = uri.getQueryParameter("oauth_token_secret")
                            params["handle"] = uri.getQueryParameter("screen_name")

                            val formbuilder = MultipartBody.Builder()

                            for ((key, value) in params) {
                                formbuilder.addFormDataPart(key, value.toString())
                            }

                            val requestBody: RequestBody = formbuilder.build()

                            saveToBackend(requestBody, "/api/social-platform-keys/twitter/", "post")
                        } else {
                            runOnUiThread {
                                kotlin.run {
                                    Toast.makeText(this@ProfileEditActivity, "Something went wrong. Try again.", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }

                override fun onFailure(fail: retrofit2.Call<ResponseBody>, t: Throwable) {
                    Toast.makeText(this@ProfileEditActivity,"Network error: We couldn't connect to Twitter, try again.", Toast.LENGTH_SHORT).show()
                }
            })
        }

        if (view !is EditText) {
            view.setOnTouchListener(OnTouchListener { v, event ->
                hideSoftKeyboard()
                false
            })
        }

        val sharedPreferences = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        authToken = sharedPreferences.getString(getString(R.string.ftm_user_token), "")

        if (authToken == "") {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val client = OkHttpClient()

        val post: Request = Request.Builder()
                .url(getString(R.string.domain) + "/api/profile")
                .header("Authorization", "Token $authToken")
                .cacheControl(CacheControl.Builder().noCache().build())
                .build()

        client.newCall(post).enqueue(object : okhttp3.Callback {
            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
                val responseBody: String = response.body!!.string()
                if (!response.isSuccessful) {
                    throw IOException("Unexpected code $response")
                }
                if (responseBody != null) {
                    val json = JSONObject(responseBody)
                    truckName = if (!json.isNull("truck_name")) json.getString("truck_name") else ""
                    truckDescription = if(!json.isNull("truck_description")) json.getString("truck_description") else ""
                    truckWebsite = if(!json.isNull("truck_website")) json.getString("truck_website") else ""
                    truckImage = if(!json.isNull("truck_image")) json.getString("truck_image") else ""
                    truckTwitterAccount = if(!json.isNull("twitter_account")) json.getString("twitter_account") else ""
                    baseContext.mainLooper.run {
                        setProfileValues()
                    }
                }
            }

            override fun onFailure(call: okhttp3.Call, e: IOException) {
                runOnUiThread {
                    kotlin.run {
                        Toast.makeText(this@ProfileEditActivity, "Network error", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun Activity.hideSoftKeyboard() {
        currentFocus?.let {
            val inputMethodManager = ContextCompat.getSystemService(this, InputMethodManager::class.java)!!
            inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
            currentFocus.clearFocus()
        }
    }

    fun backClicked(view: View) {
        goBack()
    }

    fun goBack() {
        val intent = Intent(this, CheckInActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun saveClicked(view: View) {
        truckName = findViewById<EditText>(R.id.editable_truck_name).text.toString()
        truckWebsite = findViewById<EditText>(R.id.truck_website).text.toString()
        truckDescription = findViewById<EditText>(R.id.truck_description).text.toString()

        var params = HashMap<String, Any>()

        val formbuilder = MultipartBody.Builder()

        if (newTruckImage != null) {
            //val file = File(newTruckImage!!.path)
            val file = File(getRealPathFromURI(newTruckImage!!));
            formbuilder.setType(MultipartBody.FORM)
            formbuilder.addFormDataPart("truck_image", "truck_image.png",
                    file.readBytes().toRequestBody("image/png".toMediaTypeOrNull(), 0, file.readBytes().size))
        }

        params["truck_name"] = truckName
        params["truck_description"] = truckDescription
        params["truck_website"] = truckWebsite

        for ((key, value) in params) {
            formbuilder.addFormDataPart(key, value.toString())
        }

        val requestBody: RequestBody = formbuilder.build()

        saveToBackend(requestBody, "/api/profile/", "put")

    }

    private fun getRealPathFromURI(contentURI: Uri): String? {
        val result: String
        val cursor: Cursor? = contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun uploadPhoto(view: View) {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)

        startActivityForResult(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 3)
    }

    private fun saveToBackend(requestBody: RequestBody, path: String, verb: String) {

        val client = OkHttpClient()

        val request = Request.Builder()
                .url(getString(R.string.domain) + path)
                .header("Authorization", "Token $authToken")

        if (verb == "post") {
            request.post(requestBody)
        } else {
            request.put(requestBody)
        }

        client.newCall(request.build()).enqueue(object : okhttp3.Callback {
            override fun onFailure(call: okhttp3.Call, e: IOException) {
                runOnUiThread {
                    kotlin.run {
                        Toast.makeText(this@ProfileEditActivity, e.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
                baseContext.mainLooper.run {
                    goBack()
                }
            }
        })

    }

    fun loginToTwitter(view: View) {
        val oauthHeader = generateOAuthAuthorizationHeader(null)

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.twitter.com/")
                .build()
        retrofit.callbackExecutor()
        val service = retrofit.create(AuthenticationService::class.java)
        val call = service.getRequestToken(oauthHeader)
        call.enqueue( object: retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: retrofit2.Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val responseBody = response.body()!!.string()
                    val regex = "oauth_token=(.*)&oauth_token_secret".toRegex()
                    twitterAccessToken = regex.find(responseBody)!!.value.replace("oauth_token=","").replace("&oauth_token_secret","")
                    initializeAppAuth()
                } else {
                    // There was an error processing the authorization request params
                }
            }

            override fun onFailure(fail: retrofit2.Call<ResponseBody>, t: Throwable) {
                Toast.makeText(this@ProfileEditActivity,"Network error: We couldn't connect to Twitter, try again.", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun setProfileValues() {
        runOnUiThread(Runnable {
            val truckImageView: ImageView = findViewById(R.id.truck_image)
            if (!truckImage.isNullOrBlank()) {
                Picasso.get().load(truckImage).into(truckImageView)
            }

            val truckNameView: TextView = findViewById(R.id.truck_name)
            truckNameView.text = truckName

            val editableTruckNameView: EditText = findViewById(R.id.editable_truck_name)
            editableTruckNameView.text = Editable.Factory.getInstance().newEditable(truckName)

            val truckWebsiteView: EditText = findViewById(R.id.truck_website)
            truckWebsiteView.text = Editable.Factory.getInstance().newEditable(truckWebsite)

            val truckTwitterView: TextView = findViewById(R.id.truck_twitter)
            truckTwitterView.text = if(!truckTwitterAccount.isNullOrBlank()) "@$truckTwitterAccount" else ""

            val truckDescriptionView: EditText = findViewById(R.id.truck_description)
            truckDescriptionView.text = Editable.Factory.getInstance().newEditable(truckDescription)
        })
    }

    private fun generateOAuthAuthorizationHeader(extraParams: HashMap<String,String>?): String {
        val timeStamp = System.currentTimeMillis()/1000
        val nonce = UUID.randomUUID().toString().replace("-","")
        val params = HashMap<String,String>()
        params["oauth_callback"] = redirectUrl
        params["oauth_consumer_key"] = getString(R.string.TWITTER_KEY)
        params["oauth_nonce"] = nonce
        params["oauth_signature_method"] = "HMAC-SHA1"
        params["oauth_timestamp"] = timeStamp.toString()
        params["oauth_version"] = "1.0"

        if(extraParams != null){
            params.putAll(extraParams)
        }

        return generateOAuthHeader(params,requestTokenUrl)
    }

    private fun generateOAuthHeader(params: HashMap<String,String>, endpoint: String) : String{
        val encodedParams = ArrayList<String>()
        params.forEach { (key, value) -> encodedParams.add(percentEncode(key)+"="+percentEncode(value)) }
        params.toSortedMap()

        Collections.sort(encodedParams, String.CASE_INSENSITIVE_ORDER)

        val paramsString = encodedParams.joinToString("&")
        val baseString = "POST&${percentEncode(endpoint)}&${percentEncode(paramsString)}"
        val signingKey = percentEncode(getString(R.string.TWITTER_SECRET))+"&"

        params["oauth_signature"] = calculateRFC2104HMAC(baseString, signingKey)

        val encodedHeaderValues= ArrayList<String>()
        params.forEach { (key, value) -> encodedHeaderValues.add(percentEncode(key)+"="+"\""+percentEncode(value)+ "\"") }

        return "OAuth ${encodedHeaderValues.joinToString(", ")}"
    }

    private fun percentEncode(s: String?): String {
        if (s == null) {
            return ""
        }
        try {
            return URLEncoder.encode(s, "UTF-8")
                    // OAuth encodes some characters differently:
                    .replace("+", "%20").replace("*", "%2A")
                    .replace("%7E", "~")
            // This could be done faster with more hand-crafted code.
        } catch (wow: UnsupportedEncodingException) {
            throw RuntimeException(wow.message, wow)
        }
    }

    private fun calculateRFC2104HMAC(data: String, key: String): String {
        val signingKey = SecretKeySpec(key.toByteArray(), "HmacSHA1")
        val mac = Mac.getInstance("HmacSHA1")
        mac.init(signingKey)
        return android.util.Base64.encodeToString(mac.doFinal(data.toByteArray()),android.util.Base64.DEFAULT)
    }

    @WorkerThread
    private fun initializeAppAuth() {
        val config = AuthorizationServiceConfiguration(Uri.parse(authorizeUrl), Uri.parse(accessTokenUrl))
        mAuthStateManager.replace(AuthState(config))
        initializeClient()
        return
    }

    @WorkerThread
    private fun initializeClient() {
        mClientId.set(getString(R.string.TWITTER_KEY))
        runOnUiThread { createAuthRequest() }
        return
    }

    private fun createAuthRequest() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://api.twitter.com/oauth/authorize?oauth_token=$twitterAccessToken"))
        startActivity(browserIntent)

    }

    @MainThread
    private fun startAuth() {
        mExecutor?.submit { doAuth() }
    }

    @WorkerThread
    private fun doAuth() {
        val intent = mAuthService?.getAuthorizationRequestIntent(mAuthRequest.get(), mAuthIntent.get())
        startActivityForResult(intent, 100)
    }

}