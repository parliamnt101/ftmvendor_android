package foodtruckmovement.ftmvendor.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.amplifyframework.AmplifyException
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.core.Amplify
import com.foodtruckmovement.ftmvendor.R
import foodtruckmovement.ftmvendor.ui.checkIn.CheckInActivity
import foodtruckmovement.ftmvendor.ui.login.LoginActivity

class LauncherActivity : AppCompatActivity() {
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Amplify.addPlugin(AWSCognitoAuthPlugin())

        try {
            Amplify.configure(applicationContext)
            Log.i("FTM", "Initialized Amplify")
        } catch (error: AmplifyException) {
            Log.e("FTM", "Could not initialize Amplify", error)
        }

        val sharedPreferences = this.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        val authToken = sharedPreferences.getString(getString(R.string.ftm_user_token), "")

        if (authToken == "") {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, CheckInActivity::class.java)
            startActivity(intent)
        }

        finish()
    }
}