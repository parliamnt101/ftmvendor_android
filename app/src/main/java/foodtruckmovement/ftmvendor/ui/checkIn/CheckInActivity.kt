package foodtruckmovement.ftmvendor.ui.checkIn

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.drawerlayout.widget.DrawerLayout.SimpleDrawerListener
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.foodtruckmovement.ftmvendor.R
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.squareup.picasso.Picasso
import foodtruckmovement.ftmvendor.ui.login.LoginActivity
import foodtruckmovement.ftmvendor.ui.profile.ProfileEditActivity
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.util.*


class CheckInActivity : AppCompatActivity(), OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener{


    private lateinit var mMap: GoogleMap

    private var authToken = ""
    private var truckName = ""
    private var truckDescription = ""
    private var truckWebsite = ""
    private var truckImage = ""
    private var truckTwitterAccount = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPreferences = this@CheckInActivity?.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        this.authToken = sharedPreferences.getString(getString(R.string.ftm_user_token), "")

        if (authToken == "") {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val client = OkHttpClient()

        val post: Request = Request.Builder()
                .url(getString(R.string.domain) + "/api/profile")
                .header("Authorization", "Bearer $authToken")
                .build()

        client.newCall(post).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val responseBody: ResponseBody? = response.body
                if (!response.isSuccessful) {
                    throw IOException("Unexpected code $response")
                }
                if (responseBody != null) {
                    val json = JSONObject(responseBody.string())
                    truckName = if (!json.isNull("truck_name")) json.getString("truck_name") else ""
                    truckDescription = if(!json.isNull("truck_description")) json.getString("truck_description") else ""
                    truckWebsite = if(!json.isNull("truck_website")) json.getString("truck_website") else ""
                    truckImage = if(!json.isNull("truck_image")) json.getString("truck_image") else ""
                    truckTwitterAccount = if(!json.isNull("twitter_account")) json.getString("twitter_account") else ""
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    kotlin.run {
                        Toast.makeText(this@CheckInActivity, "Network error", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })

        setContentView(R.layout.activity_check_in)

        val mDrawerLayout = findViewById<DrawerLayout>(R.id.checkin_drawer_layout)
        mDrawerLayout.addDrawerListener(object : SimpleDrawerListener() {
            override fun onDrawerStateChanged(newState: Int) {
                if (newState == DrawerLayout.STATE_DRAGGING && !mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                    setMenuValues()
                }
            }
        })

        /*val config = TwitterConfig.Builder(this)
                .logger(DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(TwitterAuthConfig(twitterKey, twitterSecret))
                .debug(true)
                .build();
        Twitter.initialize(config)*/

        /* if (TwitterCore.getInstance().getSessionManager().getActiveSession() != null) {
             loginButton.setVisibility(View.GONE)
         }*/

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)

        /*loginButton.setCallback(object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                Toast.makeText(this@CheckInActivity, "GET IT", Toast.LENGTH_SHORT).show()
                TweetComposer.Builder(this@CheckInActivity).text("testing 123")
            }

            override fun failure(exception: TwitterException) {
                Toast.makeText(this@CheckInActivity, "Fuck It", Toast.LENGTH_SHORT).show()
                Log.d("twitter error: ","Something went wrong, please try again")
            }
        })*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Pass the activity result to the login button.
        //loginButton.onActivityResult(requestCode, resultCode, data)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true;
            mMap.uiSettings.isMyLocationButtonEnabled = true;
            mMap.setOnMyLocationButtonClickListener(this);
            mMap.setOnMyLocationClickListener(this);

            val mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            mFusedLocationProviderClient.lastLocation
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            val mLastKnownLocation = task.result
                            if (mLastKnownLocation != null) {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        LatLng(mLastKnownLocation.latitude,
                                                mLastKnownLocation.longitude), 17.0f))
                            }
                        }
                    }
        } else {
            Toast.makeText(this@CheckInActivity, "I need your location, dummy!", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onMyLocationButtonClick(): Boolean {
        //Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    override fun onMyLocationClick(location: Location) {
        //Toast.makeText(this, "Current location:\n$location", Toast.LENGTH_LONG).show()
    }


    fun checkIn(view: View) {
        val geocoder = Geocoder(this, Locale.getDefault())
        val latitude = mMap.cameraPosition.target.latitude
        val longitude = mMap.cameraPosition.target.longitude
        val rawAddress = geocoder.getFromLocation(latitude, longitude, 1)[0]
        val addressFragments = with(rawAddress) {
            (0..maxAddressLineIndex).map { getAddressLine(it) }
        }
        val address = addressFragments.joinToString(separator = "\n")

        val client = OkHttpClient()

        val formBody: RequestBody = FormBody.Builder()
                .add("latitude", latitude.toString())
                .add("longitude", longitude.toString())
                .add("status", "testing123456")
                .add("address", address)
                .build()

        val post: Request = Request.Builder()
                .url(getString(R.string.domain) + "/api/check-in/")
                .header("Authorization", "Bearer $authToken")
                .post(formBody)
                .cacheControl(CacheControl.Builder().noCache().build())
                .build()

        client.newCall(post).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                Toast.makeText(this@CheckInActivity, "Checked in", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    kotlin.run {
                        Toast.makeText(this@CheckInActivity, "Network error", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    fun menuCLicked(view: View) {
        val mDrawerLayout = findViewById<DrawerLayout>(R.id.checkin_drawer_layout)
        val mDrawerContent = findViewById<LinearLayout>(R.id.checkin_side_bar)
        mDrawerLayout.openDrawer(mDrawerContent, true)

        setMenuValues()

    }

    @SuppressLint("SetTextI18n")
    private fun setMenuValues() {
        val truckImageView: ImageView = findViewById(R.id.truck_image)
        if (!truckImage.isNullOrBlank()) {
            Picasso.get().load(truckImage).into(truckImageView)
        }

        val truckNameView: TextView = findViewById(R.id.truck_name)
        truckNameView.text = truckName

        val truckWebsiteView: TextView = findViewById(R.id.truck_website)
        truckWebsiteView.text = truckWebsite

        val truckTwitterView: TextView = findViewById(R.id.truck_twitter)
        truckTwitterView.text = if(!truckTwitterAccount.isNullOrBlank()) "@$truckTwitterAccount" else ""

        val truckDescriptionView: TextView = findViewById(R.id.truck_description)
        truckDescriptionView.text = truckDescription
    }

    fun logout(view: View) {
        val sharedPreferences = this@CheckInActivity?.getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        with (sharedPreferences.edit()) {
            putString(getString(R.string.ftm_user_token), "")
            commit()
        }

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun navigateToProfileActivity(view: View) {
        val intent = Intent(this, ProfileEditActivity::class.java)
        startActivity(intent)
    }
}

